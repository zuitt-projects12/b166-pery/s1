// Every node module is an object.
const { assert } = require("chai"); 
const { newUser } = require("../index");

/* 
    describe(name, () => {
        it("name", () => {
            assert.equal(typeof(newUser), "string" || "boolean"); // what should happen
        }); // specify test case
    }); // handles the test suite
*/
describe("Test newUser object", () => {
    it("Assert newUser type is an object", () => {
        assert.equal(typeof(newUser), "object"); 
    });
    it("Assert newUser email is type string", () => {
        assert.equal(typeof(newUser.email), "string");
    });
    it("Assert that newUser email is not undefined", () => {
        assert.notEqual(typeof(newUser.email), "undefined");
    });
    it("Assert newUser password is type string", () => {
        assert.equal(typeof(newUser.password), "string");
    });
    it("Assert newUser password has at least 16 characters", () => {
        assert.isAtLeast(newUser.password.length, 16);
    });
});

describe("Activity Test", () => {
    it("Assert newUser firstName is type string", () => {
        assert.equal(typeof(newUser.firstName), "string");
    });
    it("Assert newUser lastName is type string", () => {
        assert.equal(typeof(newUser.lastName), "string");
    });
    it("Assert newUser firstName is not undefined", () => {
        assert.notEqual(typeof(newUser.firstName), "undefined");
    });
    it("Assert newUser lastName is not undefined", () => {
        assert.notEqual(typeof(newUser.lastName), "undefined");
    });
    it("Assert newUser age is at least 18", () => {
        assert.isAtLeast(newUser.age, 18);
    });
    it("Assert newUser age is type number", () => {
        assert.equal(typeof(newUser.age), "number");
    });
    it("Assert newUser contactNo is type string", () => {
        assert.equal(typeof(newUser.contactNo), "string");
    });
    it("Assert newUser batchNo is type number", () => {
        assert.equal(typeof(newUser.batchNo), "number");
    });
    it("Assert newUser batchNo is not undefined", () => {
        assert.notEqual(typeof(newUser.batchNo), "undefined");
    });
});
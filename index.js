const express = require("express");
const app = express();
const PORT = process.env.PORT || 5000;

app.use(express.json());

const newUser = {
    firstName: "levi",
    lastName: "ackerman",
    age: 28,
    contactNo: "09171234567",
    batchNo: 166,
    email: "leviAckerman@mail.com",
    password: "thequickbrownfoxjumpsoverthelazydog"
};

module.exports = {
    // This is to export to other module.
    newUser: newUser
};

// app.get("/", (req, res) => res.send("Hello Batch 166!"));


app.listen(PORT, () => console.log(`Server running on port ${PORT}`));